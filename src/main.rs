#[macro_use]
extern crate serde_derive;
extern crate chrono;
extern crate jsonwebtoken;

use chrono::{DateTime, NaiveDateTime, Utc};
use jsonwebtoken::{decode, decode_header, DecodingKey, TokenData, Validation, Algorithm};
use std::env;
use std::env::home_dir;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
struct Claims {
    sub: String,
    exp: i64,
}

fn secret_from_rc() -> String {
    let rc_filename = ".jwtexp.rc";
    let rc_path = home_dir().unwrap().join(rc_filename);
    let file =
        BufReader::new(File::open(&rc_path).expect(&format!("Failed to read {}", rc_filename)));
    let mut secret_iter = file.lines().into_iter().take(1);
    let secret = secret_iter
        .next()
        .expect("Failed to read first line of ~/.jwtexp.rc")
        .expect("Failed to read ~/.jwtexp.rc");
    secret
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let usage = r#"
    jwtexp -h
    jwtexp <secret> <token>
    jwtexp <token> # store your secret in ~/.jwtexp.rc
    "#;
    if args[1] == "-h" {
        println!("{}", &usage);
        return;
    }
    let secret: String;
    let token: String;
    match args.len() {
        3 => {
            secret = args[1].clone();
            token = args[2].clone();
        }
        2 => {
            secret = secret_from_rc();
            token = args[1].clone();
        }
        _ => {
            println!("Wrong number of args. Check `jwtexpt -h`");
            return;
        }
    }

    let header = decode_header(&token).expect("Failed to decode token.");
    let token_decoded = decode::<Claims>(
        &token,
        &DecodingKey::from_secret(secret.as_ref()),
        &Validation::new(Algorithm::HS256),
    ).expect("Failed to decode token claim. Make sure you have `sub` and `exp` in your token claim.");
    println!("{:?}", token_decoded);
    let expire_date: DateTime<Utc> = DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(token_decoded.claims.exp, 0), Utc);
    println!("Token expiration date: {:?}", &expire_date);
}
